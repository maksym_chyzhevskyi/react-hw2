import React, { Component } from "react";
import styles from "./styles/app.module.scss";

import { Service } from "./components/services";
import { Goods } from "./components/goods";
import { Header } from "./components/header";

export class App extends Component {
  constructor() {
    super();

    this.state = {
      error: null,
      isLoaded: false,
      sneakers: [],
      goodsFavoriteId: [],
      goodsCartId: [],
    };
  }

  componentDidMount() {
    Service.getAll()
      .then((response) => response.json())
      .then(
        (result) => {
          this.setState({ isLoaded: true, sneakers: result.sneakers });
        },
        (error) => {
          this.setState({ isLoaded: true, error });
        }
      );

    this.getStorageValue("goodsFavoriteId");
    this.getStorageValue("goodsCartId");
  }

  getStorageValue = (name) => {
    const storageId = localStorage.getItem(name);
    if (storageId) {
      const newStateValue = storageId
        .split(",")
        .map((storageId) => Number(storageId));
      this.setState({ [name]: newStateValue });
    }
  };

  addStorageValue = (name, id) => {
    const newStateValue = this.state[name];
    newStateValue.push(id);
    this.setState({ [name]: newStateValue });
    localStorage.setItem(name, newStateValue);
  };

  removeStorageValue = (name, id) => {
    let newStateValue = [];
    const oldStateValue = this.state[name];
    if (oldStateValue.length) {
      newStateValue = oldStateValue.filter((storageId) => storageId !== id);
      this.setState({ [name]: newStateValue });
      localStorage.setItem(name, newStateValue);
    }
  };

  render() {
    const { error, isLoaded, sneakers } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className={styles.App}>
          <Header
            goodsFavoriteCount={this.state.goodsFavoriteId.length}
            goodsCartCount={this.state.goodsCartId.length}
          />
          <Goods
            sneakers={sneakers}
            addStorageValue={this.addStorageValue}
            removeStorageValue={this.removeStorageValue}
          />
        </div>
      );
    }
  }
}
