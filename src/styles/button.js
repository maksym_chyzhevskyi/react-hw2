import styled from "styled-components";

export const MyButton = styled.button`
  cursor: pointer;
  min-width: 100px;
  border-radius: 5px;
  min-height: 40px;
  padding: 10px 15px;
  color: #fff;
  text-transform: uppercase;
  background-color: ${({ className }) => {
    switch (className) {
      case "red":
        return "#b3382c";
      case "green":
        return "#34ab40";
      case "blue":
        return "#5661e1";
      default:
        return "#e74c3c";
    }
  }};
`;
