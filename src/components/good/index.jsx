import React, { Component } from "react";
import styles from "../../styles/good.module.scss";
import PropTypes from "prop-types";

import { Button } from "../button";
import { Modal } from "../modal";

export class Good extends Component {
  constructor(props) {
    super(props);
    this.item = props.item;
    this.state = {
      addToCart: false,
      goodsFavoriteId: false,
      goodsCartId: false,
    };
  }

  render() {
    const { id, name, price, imgUrl, article, color } = this.item;
    return (
      <>
        <div className={styles.GoodContainer}>
          <div className={styles.Good}>
            <div className={styles.body}>
              <div className={styles.img}>
                <img src={`./img/${imgUrl}`} alt="" />
              </div>
              <div className={styles.title}>{name}</div>
              <div className={styles.article}>
                <span className={styles.articleText}>Article</span>:&nbsp;
                <span className={styles.articleValue}>{article}</span>
              </div>
              <div className={styles.color}>
                <span className={styles.colorText}>Color</span>:&nbsp;
                <span className={styles.colorValue}>{color}</span>
              </div>
            </div>
            <div className={styles.bottom}>
              <div className={styles.actions}>
                <span className={styles.cart}>
                  {!this.state.goodsCartId ? (
                    <i
                      className="fa-solid fa-circle-plus"
                      onClick={() => {
                        this.setState({ addToCart: true });
                      }}
                    ></i>
                  ) : (
                    <i
                      className="fa-solid fa-basket-shopping"
                      onClick={() => {
                        this.setState({ goodsCartId: false });
                        this.props.removeStorageValue("goodsCartId", id);
                      }}
                    ></i>
                  )}
                </span>
                <span className={styles.favorites}>
                  {!this.state.goodsFavoriteId ? (
                    <i
                      className="fa-regular fa-star"
                      onClick={() => {
                        this.setState({ goodsFavoriteId: true });
                        this.props.addStorageValue("goodsFavoriteId", id);
                      }}
                    ></i>
                  ) : (
                    <i
                      className="fa-solid fa-star"
                      onClick={() => {
                        this.setState({ goodsFavoriteId: false });
                        this.props.removeStorageValue("goodsFavoriteId", id);
                      }}
                    ></i>
                  )}
                </span>
              </div>
              <div className={styles.price}>
                <span className={styles.sum}>{price}</span>
                <span className={styles.currency}> usd</span>
              </div>
            </div>
          </div>
        </div>
        {this.state.addToCart && (
          <Modal
            className={"blue"}
            setVisible={() => this.setState({ addtoCart: false })}
            header={<>Do you want to add the item to the cart?</>}
            closeButton={false}
            text={
              <div className={styles.modalAddCart}>
                <div className={styles.title}>{name}</div>
                <div className={styles.price}>
                  <span className={styles.sum}>{price}</span>
                  <span className={styles.currency}> usd</span>
                </div>
              </div>
            }
            action={
              <>
                <Button
                  className={"blue"}
                  text="OK"
                  backgroundColor="#424BB2"
                  onClick={() => {
                    this.setState({ goodsCartId: true });
                    this.props.addStorageValue("goodsCartId", id);
                    this.setState({ addToCart: false });
                  }}
                />
                <Button
                  className={"blue"}
                  text="Cancel"
                  backgroundColor="#424BB2"
                  onClick={() => this.setState({ addToCart: false })}
                />
              </>
            }
          />
        )}
      </>
    );
  }
}

Good.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    imgUrl: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
  }).isRequired,

  addStorageValue: PropTypes.func.isRequired,
  removeStorageValue: PropTypes.func.isRequired,
};

Good.defaultProps = {
  item: {
    name: "",
    price: 0,
    imgUrl: "",
    article: "",
    color: "",
  },
};
